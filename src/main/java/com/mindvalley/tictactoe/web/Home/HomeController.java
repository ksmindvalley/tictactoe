package com.mindvalley.tictactoe.web.Home;

import com.google.gson.Gson;
import com.mindvalley.tictactoe.rules.WinRule3x3Enum;
import com.mindvalley.tictactoe.rules.WinRule4x4Enum;
import com.mindvalley.tictactoe.rules.WinRule5x5Enum;
import com.mindvalley.tictactoe.web.Home.winning.WinningReq;
import com.mindvalley.tictactoe.web.Home.winning.WinningResp;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by kaisiang on 13/03/2016.
 */
@Controller
public class HomeController {
    private static final Logger logger = LoggerFactory.getLogger(HomeController.class);

    @RequestMapping(value="/", method = RequestMethod.GET)
    public ModelAndView home(ModelAndView mav) {
        mav.setViewName("/");
        return mav;
    }

    @RequestMapping(value="/winning", method = RequestMethod.POST)
    @ResponseBody
    public String winning(ModelAndView mav, @RequestParam(value = "data", required = true) String data) {
        WinningReq req = new Gson().fromJson(data, WinningReq.class);
        WinningResp resp = new WinningResp();
        boolean win = false;

        if(req.getMode() == 3){
            Integer[] array = new Integer[req.getUser().size()];
            array = req.getUser().toArray(array);
            win = WinRule3x3Enum.getWinning(array);
        }
        else if(req.getMode() == 4){
            Integer[] array = new Integer[req.getUser().size()];
            array = req.getUser().toArray(array);
            win = WinRule4x4Enum.getWinning(array);
        }
        else if(req.getMode() == 5){
            Integer[] array = new Integer[req.getUser().size()];
            array = req.getUser().toArray(array);
            win = WinRule5x5Enum.getWinning(array);
        }

        if(win){
            resp.setHasUserWin(true);
            return new Gson().toJson(resp);
        }
        else{
            resp.setHasUserWin(false);
            Integer[] computer = new Integer[req.getComputer().size()];
            computer = req.getComputer().toArray(computer);

            Integer[] user = new Integer[req.getUser().size()];
            user = req.getUser().toArray(computer);

            if(req.getMode() == 3){
                resp.setComLastStep(new ArrayList<Integer>(Arrays.asList(WinRule3x3Enum.getLastStep(computer, user))));
            }
            else if(req.getMode() == 4){
                resp.setComLastStep(new ArrayList<Integer>(Arrays.asList(WinRule4x4Enum.getLastStep(computer, user))));
            }
            else if(req.getMode() == 5){
                resp.setComLastStep(new ArrayList<Integer>(Arrays.asList(WinRule5x5Enum.getLastStep(computer, user))));
            }

            if(resp.getComLastStep().isEmpty()){
                Integer[] userLastStep = null;
                if(req.getMode() == 3){
                    userLastStep = WinRule3x3Enum.getLastStep(user, computer);
                }
                else if(req.getMode() == 4){
                    userLastStep = WinRule4x4Enum.getLastStep(user, computer);
                }
                else if(req.getMode() == 5){
                    userLastStep = WinRule5x5Enum.getLastStep(user, computer);
                }

                if(userLastStep != null && userLastStep.length > 0){
                    resp.setComMostDuplicate(userLastStep[0]);
                    return new Gson().toJson(resp);
                }
                else {
                    Integer[] predictStep = null;
                    if(req.getMode() == 3){
                        predictStep = WinRule3x3Enum.predictStep(computer, user, WinRule3x3Enum.getNextDoubleStep(user, computer));
                    }
                    else if(req.getMode() == 4){
                        predictStep = WinRule4x4Enum.predictStep(computer, user, WinRule4x4Enum.getNextDoubleStep(user, computer));
                    }
                    else if(req.getMode() == 5){
                        predictStep = WinRule5x5Enum.predictStep(computer, user, WinRule5x5Enum.getNextDoubleStep(user, computer));
                    }

                    if(predictStep != null && predictStep.length != 0){
                        resp.setComMostDuplicate(predictStep[0]);
                    }
                    else {
                        if(req.getMode() == 3){
                            resp.setComMostDuplicate(WinRule3x3Enum.getMostDuplicate(computer, user));
                        }
                        else if(req.getMode() == 4){
                            resp.setComMostDuplicate(WinRule4x4Enum.getMostDuplicate(computer, user));
                        }
                        else if(req.getMode() == 5){
                            resp.setComMostDuplicate(WinRule5x5Enum.getMostDuplicate(computer, user));
                        }
                    }

                    return new Gson().toJson(resp);
                }
            }
            else {
                return new Gson().toJson(resp);
            }
        }
    }
}
