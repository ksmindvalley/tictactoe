package com.mindvalley.tictactoe.web.Home.winning;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by kaisiang on 14/03/2016.
 */
public class WinningReq {
    @Getter @Setter
    List<Integer> user = new ArrayList<Integer>();

    @Getter @Setter
    List<Integer> computer = new ArrayList<Integer>();

    @Getter @Setter
    int mode;
}
