package com.mindvalley.tictactoe.web.Home.winning;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by kaisiang on 14/03/2016.
 */
public class WinningResp {
    @Getter @Setter
    List<Integer> comLastStep = new ArrayList<Integer>();

    @Getter @Setter
    int comMostDuplicate;

    @Getter @Setter
    boolean hasUserWin;
}
