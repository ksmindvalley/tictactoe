package com.mindvalley.tictactoe.rules;

import com.google.gson.Gson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

/**
 * Created by kaisiang on 13/03/2016.
 */
public enum WinRule3x3Enum {
    ROW1("R1", new Integer[]{1, 2, 3}),
    ROW2("R2", new Integer[]{4, 5, 6}),
    ROW3("R3", new Integer[]{7, 8, 9}),
    COL1("C1", new Integer[]{1, 4, 7}),
    COL2("C2", new Integer[]{2, 5, 8}),
    COL3("C3", new Integer[]{3, 6, 9}),
    CROSS1("X1", new Integer[]{1, 5, 9}),
    CROSS2("X2", new Integer[]{3, 5, 7});

    private static final Logger logger = LoggerFactory.getLogger(WinRule3x3Enum.class);

    private String rulename;
    private Integer[] combination;
    private static Integer[] allNumbers = new Integer[]{1, 2, 3, 4, 5, 6, 7, 8, 9};

    private WinRule3x3Enum(String rulename, Integer[] combination) {
        this.rulename = rulename;
        this.combination = Arrays.copyOf(combination, combination.length);;
    }

    public static boolean getWinning(Integer[] numbers){
        Arrays.sort(numbers);
        List<WinRule3x3Enum> winRule3x3EnumList = new ArrayList<>(Arrays.asList(WinRule3x3Enum.class.getEnumConstants()));
        boolean winning = false;

        for(WinRule3x3Enum entry : winRule3x3EnumList){
            boolean check = true;

            for(int number : entry.combination){
                if(!Arrays.asList(numbers).contains(number)){
                    check = false;
                }
            }

            if(check){
                winning = true;
                break;
            }
        }

        return winning;
    }

    public static Integer[] getLastStep(Integer[] self, Integer[] opponent){
        List<WinRule3x3Enum> winRule3x3EnumList = new ArrayList<>(Arrays.asList(WinRule3x3Enum.class.getEnumConstants()));
        List<Integer> lastStep = new ArrayList<Integer>();

        for(WinRule3x3Enum entry : winRule3x3EnumList){
            int cnt = 0;
            int lastNum = 0;

            for(int number : entry.combination){
                if(Arrays.asList(self).contains(number)){
                    cnt++;
                }
                else{
                    lastNum = number;
                }
            }

            if(cnt == 2){
                if(!Arrays.asList(opponent).contains(lastNum)){
                    lastStep.add(lastNum);
                }
            }
        }

        Integer[] array = new Integer[lastStep.size()];
        array = lastStep.toArray(array);
        Arrays.sort(array);
        return array;
    }

    public static int getMostDuplicate(Integer[] self, Integer[] opponent){
        List<WinRule3x3Enum> winRule3x3EnumList = new ArrayList<>(Arrays.asList(WinRule3x3Enum.class.getEnumConstants()));
        List<Integer> candidate = new ArrayList<Integer>();

        for(WinRule3x3Enum entry : winRule3x3EnumList){
            for(int number : entry.combination){
                if(!Arrays.asList(self).contains(number) && !Arrays.asList(opponent).contains(number)){
                    candidate.add(number);
                }
            }
        }

        Map<Integer, Integer> map = new HashMap<Integer, Integer>();
        for (int i : candidate) {
            Integer count = map.get(i);
            map.put(i, count != null ? count+1 : 0);
        }

        Integer popular = 0;

        if(!map.isEmpty()) {
            popular = Collections.max(map.entrySet(),
                    new Comparator<Map.Entry<Integer, Integer>>() {
                        @Override
                        public int compare(Map.Entry<Integer, Integer> o1, Map.Entry<Integer, Integer> o2) {
                            return o1.getValue().compareTo(o2.getValue());
                        }
                    }).getKey();
        }

        return popular;
    }

    private static boolean nextDoubleStep(Integer[] self, Integer[] opponent, Integer nextStep){
        List<Integer> modifiedSelf = new ArrayList<Integer>(Arrays.asList(self));
        modifiedSelf.add(nextStep);

        Integer[] modifiedSelfArray = new Integer[modifiedSelf.size()];
        modifiedSelfArray = modifiedSelf.toArray(modifiedSelfArray);

        Integer[] lastStep = getLastStep(modifiedSelfArray, opponent);
        if(lastStep.length > 1){
            return true;
        }
        else{
            return false;
        }
    }

    public static Integer[] getNextDoubleStep(Integer[] self, Integer[] opponent){
        if(self.length <= 0){
            return new Integer[]{};
        }

        List<Integer> nextDoubleStep = new ArrayList<Integer>();

        for(Integer nextStep : allNumbers){
            if(nextDoubleStep(self, opponent, nextStep)){
                nextDoubleStep.add(nextStep);
            }
        }

        Integer[] array = new Integer[nextDoubleStep.size()];
        array = nextDoubleStep.toArray(array);
        Arrays.sort(array);
        return array;
    }

    public static Integer[] predictStep(Integer[] self, Integer[] opponent, Integer[] nextDoubleStep){
        List<WinRule3x3Enum> winRule3x3EnumList = new ArrayList<>(Arrays.asList(WinRule3x3Enum.class.getEnumConstants()));
        List<Integer> predictStep = new ArrayList<Integer>();

        for(WinRule3x3Enum entry : winRule3x3EnumList){
            int cnt = 0;
            int lastNum = 0;
            int lastNum2 = 0;

            for(int number : entry.combination){
                if(Arrays.asList(self).contains(number)){
                    cnt++;
                }
                else{
                    if(lastNum == 0){
                        lastNum = number;
                    }
                    else{
                        lastNum2 = number;
                    }
                }
            }

            if(cnt == 1){
                if(!Arrays.asList(opponent).contains(lastNum) && !Arrays.asList(opponent).contains(lastNum2) &&
                        !Arrays.asList(nextDoubleStep).contains(lastNum) && !Arrays.asList(nextDoubleStep).contains(lastNum2)){
                    predictStep.add(lastNum);
                }
            }
        }

        Integer[] array = new Integer[predictStep.size()];
        array = predictStep.toArray(array);
        Arrays.sort(array);
        return array;
    }
}
