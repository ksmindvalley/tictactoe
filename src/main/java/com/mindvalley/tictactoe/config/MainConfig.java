package com.mindvalley.tictactoe.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * Created by kaisiang on 3/9/15.
 */

@Configuration
@ComponentScan(basePackages = "com.mindvalley.tictactoe.web", excludeFilters = { @ComponentScan.Filter(Configuration.class) })
@EnableTransactionManagement
public class MainConfig {

}
