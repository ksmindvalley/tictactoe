package com.mindvalley.tictactoe.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

/**
 * Created by kaisiang on 3/9/15.
 */

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Override
    protected void configure(HttpSecurity http) throws Exception {

        http
            .csrf()
                .and()
                .authorizeRequests()
                .antMatchers("/resources/**").permitAll()
                .antMatchers("/user/**").permitAll()
                .antMatchers("/errors/**").permitAll()
                .antMatchers("/winning").permitAll()
                .antMatchers("/").permitAll()
            .anyRequest().authenticated()
                .and()
            .formLogin()
                .loginPage("/")
                .and()
                .authorizeRequests()
                .antMatchers("/favicon.ico").permitAll();
    }
}
