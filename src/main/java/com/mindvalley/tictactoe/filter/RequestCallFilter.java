package com.mindvalley.tictactoe.filter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by kaisiang on 3/9/15.
 */
public class RequestCallFilter extends OncePerRequestFilter {
    private static final Logger logger = LoggerFactory.getLogger(RequestCallFilter.class);
    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        if (!"HEAD".equals(request.getMethod())) {
            logger.info(request.getMethod() + "," + request.getRequestURL() + "," + request.getRemoteAddr() + "," + request.getRequestedSessionId() + "," + request.getQueryString());
            filterChain.doFilter(request, response);
        }
        else {
            filterChain.doFilter(new HttpMethodRequestWrapper("GET", request), response);
        }
    }

    private static class HttpMethodRequestWrapper extends HttpServletRequestWrapper {

        private final String method;

        public HttpMethodRequestWrapper(String method, HttpServletRequest request) {
            super(request);
            this.method = method;
        }

        @Override
        public String getMethod() {
            return this.method;
        }

    }
}
