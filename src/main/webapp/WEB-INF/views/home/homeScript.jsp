<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<script>
    $(function () {
        var token = $("meta[name='_csrf']").attr("content");
        var header = $("meta[name='_csrf_header']").attr("content");
        $(document).ajaxSend(function(e, xhr, options) {
            xhr.setRequestHeader(header, token);
        });
    });

    ContainerLayout();
    drawTicTacToe(3);

    $("#reset").click(function(){
        drawTicTacToe($("#choice").val());
    });

    $("#choice").change(function(){
        drawTicTacToe($("#choice").val());
    });

    function ContainerLayout(){
        $(".wrapper").css({
            width: "100%",
            height: ($(window).height() - 18) + "px"
        });

        $("#container").css({
            width: "100%",
            height: ($(".wrapper").height() - 36) + "px"
        });

        $("#drawboard").css({
            position: "relative",
            width: "100%",
            height: ($("#container").height() - 36) + "px",
            "max-width": "800px",
            "max-height": "600px"
        });

        $("#drawboard").attr({
            width: $("#drawboard").width(),
            height: $("#drawboard").height()
        });

        $("#drawboard").css({
            top: (($("#container").height() - $("#drawboard").height()) / 2) + "px",
            left: (($("#container").width() - $("#drawboard").width()) / 2) + "px"
        });
    }

    function drawTicTacToe(number){
        $(".box").unbind();
        $("#container").children().each(function(){
            if($(this).prop("tagName") != "CANVAS"){
                $(this).remove();
            }
        });

        var boxWidth = ($("#drawboard").width() - ((number - 1) * 2)) / number;
        var boxHeight = ($("#drawboard").height() - ((number - 1) * 2)) / number;

        var c = document.getElementById("drawboard");
        var ctx = c.getContext("2d");
        ctx.clearRect(0, 0, c.width, c.height);
        ctx.strokeStyle = '#000000';
        ctx.lineWidth = 2;
        ctx.beginPath();

        for(var i = 1; i < number; i++){
            ctx.moveTo(0, (boxHeight * i) + (2 * (i - 1)));
            ctx.lineTo($("#drawboard").width(), (boxHeight * i) + (2 * (i - 1)));
            ctx.stroke();

            ctx.moveTo((boxWidth * i) + (2 * (i - 1)), 0);
            ctx.lineTo((boxWidth * i) + (2 * (i - 1)), $("#drawboard").height());
            ctx.stroke();
        }

        var top = $("#drawboard").position().top;
        var left = $("#drawboard").position().left;

        for(var i = 1; i < ((number * number) + 1); i++){
            $("#container").append('<div id="box' + i + '" class="box"></div>');

            $("#box" + i).css({
                position: "absolute",
                top: top + "px",
                left: left + "px",
                width: (boxWidth - 5) + "px",
                height: (boxHeight - 5) + "px",

            });

            if((i % number) == 0){
                top += boxHeight + 2;
                left = $("#drawboard").position().left;
            }
            else{
                left += boxWidth + 2;
            }
        }

        $(".box").each(function(){
            $(this).click(function(){
                if($(this).attr("owner") == "user" || $(this).attr("owner") == "computer"){
                    return;
                }

                predictProcess($(this).attr("id").replace("box", ""));
            });
        });
    }

    var count = 0;
    function predictProcess(index){
        var user = [];
        var computer = [];

        $(".box").each(function(){
            if($(this).attr("owner") == "user"){
                user.push($(this).attr("id").replace("box", ""));
            }
            else if($(this).attr("owner") == "computer"){
                computer.push($(this).attr("id").replace("box", ""));
            }
        });

        user.sort();
        computer.sort();

        if(user.length == computer.length){
            drawCircle(index, "user");
            user.push(index);

            var data = {
                user: user,
                computer: computer,
                mode: $("#choice").val()
            }

            $.post("<spring:url value="/winning" />", {
                data: JSON.stringify(data)
            }, function (d) {
                var data = JSON.parse(d);

                if(data.hasUserWin){
                    $(".box").unbind();
                    alert("You win!");
                }
                else if(data.comLastStep.length > 0){
                    drawCross(data.comLastStep[0], "computer");
                    $(".box").unbind();
                    alert("You lose!");
                }
                else{
                    if(data.comMostDuplicate != 0){
                        drawCross(data.comMostDuplicate, "computer");
                        var count = 0;

                        $(".box").each(function(){
                            if($(this).attr("owner") != "user" && $(this).attr("owner") != "computer"){
                                count++;
                            }
                        });

                        if(count == 0){
                            alert("Draw!");
                        }
                    }
                    else{
                        alert("Draw!");
                    }
                }
            });
        }
    }

    function drawCross(index, owner){
        var boxTop = $("#box" + index).position().top - $("#drawboard").position().top;
        var boxLeft = $("#box" + index).position().left - $("#drawboard").position().left;
        var boxWidth = $("#box" + index).width();
        var boxHeight = $("#box" + index).height();

        var c = document.getElementById("drawboard");
        var ctx = c.getContext("2d");
        ctx.lineWidth = 2;
        ctx.beginPath();

        if(owner == "user"){
            ctx.strokeStyle = '#00ff00';
        }
        else if(owner == "computer"){
            ctx.strokeStyle = '#0000ff';
        }

        ctx.moveTo(boxLeft + 5, boxTop + 5);
        ctx.lineTo(boxLeft + boxWidth - 5, boxTop + boxHeight - 5);
        ctx.stroke();

        ctx.moveTo(boxLeft + 5, boxTop + boxHeight - 5);
        ctx.lineTo(boxLeft + boxWidth - 5, boxTop + 5);
        ctx.stroke();

        $("#box" + index).attr({
            owner: owner
        });
    }

    function drawCircle(index, owner){
        var boxTop = $("#box" + index).position().top - $("#drawboard").position().top;
        var boxLeft = $("#box" + index).position().left - $("#drawboard").position().left;
        var boxWidth = $("#box" + index).width();
        var boxHeight = $("#box" + index).height();

        if(boxWidth < boxHeight){
            var radius = ((boxWidth - 10) / 2);
        }
        else{
            radius = ((boxHeight - 10) / 2);
        }

        var c = document.getElementById("drawboard");
        var ctx=c.getContext("2d");

        ctx.beginPath();
        if(owner == "user"){
            ctx.strokeStyle = '#00ff00';
        }
        else if(owner == "computer"){
            ctx.strokeStyle = '#0000ff';
        }

        ctx.arc((boxLeft + (boxWidth / 2)), (boxTop + (boxHeight / 2)), radius, 0, 2*Math.PI);
        ctx.stroke();

        $("#box" + index).attr({
            owner: owner
        });
    }
</script>